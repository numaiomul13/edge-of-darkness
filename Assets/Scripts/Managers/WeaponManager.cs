﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponManager : MonoBehaviour {

	private LaserGun laserRef;
	public Image laserOverLay;
	public RectTransform laserUnderLay;
	
	private RocketGun rocketRef;
	public Image rocketOverLay;
	public RectTransform rocketUnderLay;
	
	
	const byte UNDER_WIDTH = 120;
	const byte UNDER_HEIGHT = 30;
	
	public void Init(Player _playerRef) {
		rocketRef = _playerRef.rocketRef;
		laserRef = _playerRef.laserRef;
		UiUpdate();
	}
	
	void Update() {
		UiUpdate();
	}
	
	void UiUpdate() {
		rocketOverLay.fillAmount = rocketRef.rateCounter / ( rocketRef.isAltFire ? rocketRef.altRateOfFire : rocketRef.rateOfFire);
		rocketUnderLay.gameObject.SetActive(rocketRef.isActive);

		laserOverLay.fillAmount = laserRef.rateCounter / ( laserRef.isAltFire ? laserRef.altRateOfFire : laserRef.rateOfFire);
		laserUnderLay.gameObject.SetActive(laserRef.isActive);
	}
}