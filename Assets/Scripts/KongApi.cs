﻿using UnityEngine;
using System.Collections.Generic;

public class KongApi : MonoBehaviour{
	private static KongApi instance;

	private List<ApiCalls> queue;

	bool hasLink = false;
	int userId = 0;
	string username = "Guest";
	string gameAuth = "";

	void Awake() {
		if (instance != null && instance != this) {
			Debug.LogException(new UnityException("Someone is attemting to create another Singleton!"));
			Destroy(gameObject);
			return;
		}
	}

	public static void InitApi() {
		if (instance != null) {
			return;
		}
		GameObject tmpGo = new GameObject("KongregateAPI",typeof(KongApi));
		Object.DontDestroyOnLoad(tmpGo);
		instance = tmpGo.GetComponent<KongApi>();
		instance.queue = new List<ApiCalls>();
		Application.ExternalEval(
			@"if(typeof(kongregateUnitySupport) != 'undefined'){
    		kongregateUnitySupport.initAPI('KongregateAPI', 'OnKongregateAPILoaded');
  			};"
		);

	}
	public void OnKongregateAPILoaded(string _userInfoString) {
		// We now know we're on Kongregate
		hasLink = true;

		// Split the user info up into tokens
		string[] tmpInfo = _userInfoString.Split("|"[0]);
		userId = int.Parse(tmpInfo[0]);
		username = tmpInfo[1];
		gameAuth = tmpInfo[2];
		while (queue.Count > 0) {
			Application.ExternalCall(queue[0].functionName, queue[0].args);
			queue.RemoveAt(0);
		}
		queue.Clear();
		OnStart();
	}

	public static void OnStart() {
		BaseAdd("Start");
	}
	public static void OnDeath() {
		BaseAdd("Death");
	}
	public static void OnWin() {
		BaseAdd("Win");
	}

	private static void BaseAdd(string _stat) {
		#if UNITY_EDITOR
		return;
		#endif
		if (instance == null || !instance.hasLink) {
			InitApi();
			instance.queue.Add(new ApiCalls("kongregate.stats.submit", _stat, 1));
		} else {
			Application.ExternalCall("kongregate.stats.submit", _stat, 1);
		}
	}

	private class ApiCalls {
		public string functionName;
		public object[] args;

		public ApiCalls(string _functionName, params object[] _args) {
			functionName = _functionName;
			args = _args;
		}
	}
}